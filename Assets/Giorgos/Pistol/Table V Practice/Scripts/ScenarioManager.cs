﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioManager : MonoBehaviour
{

    [SerializeField]
    private int maxNumOfEngagements = 0;
    [SerializeField]
    private float[] distances;
    private int currentTarget = -1;
    private float timer = 0f;
    private bool exposed = false;
    private float[] offsets;
    [SerializeField]
    private float stop;
    [SerializeField]
    private float moveOutOffset=10f;
    [SerializeField]
    private float timeBetweenEngagements;
    [SerializeField]
    public float[] time;
    [SerializeField]
    private GameObject[] targets;
    [SerializeField]
    private GameObject targetPrefab;
    [SerializeField]
    private Transform playerCamera;
    bool left7 = true;
    bool left7two = false;
    bool targetReady = false;
    private float speed = 3f;
    private bool leftInitial = true;
    [SerializeField]
    private int[] stopInt;
    private bool proceedToNextTarget = true;
    private bool playerInPosition = false;
    [SerializeField]
    private int[] pair;
    [SerializeField]
    private int[] threePair;
    [SerializeField]
    private int[] fourPair;
    [SerializeField]
    private Practice practiceSelection;
    private enum Practice { Pistol, Rifle };

    private Vector3 initPosition;
    public bool executeDrill = false;
    private void Start()
    {
        initPosition = playerCamera.transform.position;
        targets = new GameObject[maxNumOfEngagements];
        offsets = new float[10];
        for (int i = 0; i < maxNumOfEngagements; i++)
        {
            float dist = 0f;
            if (practiceSelection == Practice.Pistol)
            {
                switch (distances[i])
                {
                    case 7f:

                        if (leftInitial || left7two)
                        {
                            dist = -5f;
                            left7 = false;
                            leftInitial = false;
                            left7two = false;
                        }
                        else
                        {
                            dist = 5f;
                            if (left7)
                                left7two = true;
                            else
                                left7 = true;

                        }
                        break;
                    case 12.5f:
                        dist = 3f;
                        break;
                    case 23f:
                        dist = -1f;
                        break;
                    case 31f:
                        dist = 1f;
                        break;
                    case 10f:
                        dist = -3f;
                        break;
                    case 16.5f:
                        dist = -6f;
                        break;
                    case 27f:
                        dist = -2.5f;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (distances[i])
                {
                    case 50f:

                        if (leftInitial || left7two)
                        {
                            dist = 5f;
                            left7 = false;
                            leftInitial = false;
                            left7two = false;
                        }
                        else
                        {
                            dist = -5f;
                            if (left7)
                                left7two = true;
                            else
                                left7 = true;

                        }
                        break;
                    case 200f:
                        dist = 3f;
                        break;
                    case 150f:
                        dist = -1f;
                        break;
                    case 300f:
                        dist = 1f;
                        break;
                    case 100f:
                        dist = -3f;
                        break;
                    case 250f:
                        dist = -2.5f;
                        break;
                    default:
                        break;
                }
            }
            if (i <= 9)
            {
                int k = 0;
                for (; k < i; k++)
                {
                    if (dist == offsets[k])
                    {
                        targets[i] = targets[k];
                        break;
                    }

                }
                if (k == i)
                {
                    targets[i] = Instantiate(targetPrefab, playerCamera.transform.position + (distances[i] + moveOutOffset) * Vector3.forward + dist * Vector3.right, targetPrefab.transform.rotation);
                    targets[i].transform.position = new Vector3(targets[i].transform.position.x, 29.77f, targets[i].transform.position.z);
                    offsets[i] = dist;
                    targets[i].transform.LookAt(new Vector3(playerCamera.position.x, 29.77f,playerCamera.position.z));
                }
            }
            else
            {
                for (int j = 0; j <= 9; j++)
                {
                    if (dist == offsets[j])
                    {
                        targets[i] = targets[j];
                        break;
                    }
                }
            }
        }
    }
    void Update()
    {
        if (!executeDrill) { return; }
        if (executeDrill && !playerInPosition)
        {
            StartCoroutine(MovePlayer());
            return;
        }
        timer += Time.deltaTime;
        if (proceedToNextTarget)
        {
            proceedToNextTarget = false;
            timer = 0f;
            currentTarget++;
            if (currentTarget == maxNumOfEngagements)
            {
                transform.GetComponent<ScenarioManager>().enabled = false;
            }
            else
            {
                if (currentTarget == stopInt[0] || currentTarget == stopInt[1])
                    StartCoroutine(WaitTimeBetweenMagazines(stop));
                else
                    StartCoroutine(WaitTimeBetweenTargets(timeBetweenEngagements));
            }
        }
    }
    IEnumerator WaitTimeBetweenTargets(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        int i = 0;
        if (pair.Length > 0)
        {
            for (; i < pair.Length; i++)
            {
                if (currentTarget == pair[i])
                {
                    StartCoroutine(TargetsAtTheSameTime(2));
                    break;
                }

            }
        }
        int twoPairVal = i;
        if (threePair.Length > 0)
        {
            for (i = 0; i < threePair.Length; i++)
            {
                if (currentTarget == threePair[i])
                {
                    StartCoroutine(TargetsAtTheSameTime(3));
                    break;
                }

            }
        }
        int threePairVal = i;
        if (fourPair.Length > 0)
        {
            for (i = 0; i < fourPair.Length; i++)
            {
                if (currentTarget == fourPair[i])
                {
                    StartCoroutine(TargetsAtTheSameTime(4));
                    break;
                }

            }
        }
        int fourPairVal = i;
        if (twoPairVal == pair.Length || threePairVal == threePair.Length || fourPairVal == fourPair.Length)
            StartCoroutine(SetATargetActive(currentTarget));
    }

    IEnumerator SetATargetActive(int currentTarget)
    {
        targets[currentTarget].GetComponent<Animator>().SetBool("down", false);
        targets[currentTarget].GetComponent<Animator>().SetBool("up", true);
        yield return new WaitForSeconds(time[currentTarget]);
        targets[currentTarget].GetComponent<Animator>().SetBool("up", false);
        targets[currentTarget].GetComponent<Animator>().SetBool("down", true);
        proceedToNextTarget = true;
    }

    IEnumerator WaitTimeBetweenMagazines(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        StartCoroutine(WaitTimeBetweenTargets(timeBetweenEngagements));
    }
    IEnumerator TargetsAtTheSameTime(int numOfTargets)
    {
        for (int i = 0; i < numOfTargets; i++)
        {
            targets[currentTarget + i].GetComponent<Animator>().SetBool("down", false);
            targets[currentTarget + i].GetComponent<Animator>().SetBool("up", true);
        }
        yield return new WaitForSeconds(time[currentTarget]);
        for (int i = 0; i < numOfTargets; i++)
        {
            targets[currentTarget + i].GetComponent<Animator>().SetBool("up", false);
            targets[currentTarget + i].GetComponent<Animator>().SetBool("down", true);
        }

        currentTarget = currentTarget + numOfTargets - 1;
        proceedToNextTarget = true;
    }

    public GameObject GetTargetForEngagement(int targetIndex)
    {
        GameObject target = null;
        if (targetIndex < 0) return null;
        target = targets[targetIndex].GetComponent<RT_Target>().GetTargetGo();
        StartCoroutine(SetATargetActive(targetIndex));
        return target;
    }

    IEnumerator MovePlayer()
    {
        while(Mathf.Abs(initPosition.z-playerCamera.transform.position.z)<10f)
        {
            playerCamera.position += Vector3.forward *0.005f* Time.deltaTime;
            yield return null;
        }
        playerInPosition = true;
    }

}
