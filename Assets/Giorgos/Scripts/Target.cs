using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float timeToStayActivated;

    private void Start()
    {
        float distance = Vector3.Distance(this.transform.position, GameObject.FindGameObjectWithTag("ShootingPosition").transform.position);
        timeToStayActivated = distance;
        TargetController.timeForSession += distance;
    }
}
