using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController: MonoBehaviour
{
    [SerializeField]
    private GameObject targetPrefab;
    [SerializeField]
    private Transform shootingPosition;
    public static float timeForSession=0;
    private GameObject[] target = new GameObject[5];
    [SerializeField]
    private Vector3[] distanceVector;
    [SerializeField]
    private int[] orderOfTargets;
    private bool once = false;

    private void Start()
    {
        Spawn();
        StartCoroutine(SetATargetActive());
    }
    private void Spawn()
    {
        for (int i = 0; i < distanceVector.Length; i++)
        {
            Vector3 spawnPosition = new Vector3(shootingPosition.position.x + distanceVector[i].x, 0.5f, shootingPosition.position.z - distanceVector[i].z);
            Quaternion spawnRotation = Quaternion.identity;
            spawnRotation.eulerAngles = new Vector3(86.754f, 0, 0);
            target[i] = Instantiate(targetPrefab, spawnPosition, spawnRotation);
        }
    }

    private IEnumerator SetATargetActive()
    {
        yield return new WaitForEndOfFrame();
        int index = 0;
        while (index< distanceVector.Length)
        {
            target[orderOfTargets[index]].GetComponent<Animator>().SetBool("ActivateTarget", true);
            yield return new WaitForSeconds(target[orderOfTargets[index]].GetComponent<Target>().timeToStayActivated);
            target[orderOfTargets[index]].GetComponent<Animator>().SetBool("ActivateTarget", false);
            ++index;

        }
        yield return 0;
    }
}
