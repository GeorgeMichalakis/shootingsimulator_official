﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManagerTemp : MonoBehaviour
{
    [SerializeField] Camera myCam;
    [SerializeField] GameObject bulletHoleOrig;

    List<GameObject> bulletHoles = new List<GameObject>();

    [SerializeField] GameObject dirtImpactOrig;
    [SerializeField] GameObject woodImpactOrig;
    [SerializeField] GameObject metalImpactOrig;
    List<GameObject> impactParticles = new List<GameObject>();

    int shots = 0;
    int hits = 0;
    int misses = 0;
    [SerializeField] Text shotsValueText;
    [SerializeField] Text hitsValueText;
    [SerializeField] Text missesValueText;
    [SerializeField] Text timeLeftText;
    [SerializeField] Text timeLeftValueText;
    [SerializeField] Image timeIconBackground;

    const int numAudioSources = 10;
    GameObject[] bulletHitAudioSources = new GameObject[numAudioSources];
    int currentAudioSourceIndex = 0;
    [SerializeField] GameObject bulletHitAudioSourceOrig;
    [SerializeField] AudioClip dirtImpactAudioClip;
    [SerializeField] AudioClip woodImpactAudioClip;
    [SerializeField] AudioClip metalImpactAudioClip;

  //  const float initialTimeLeft = 10.0f;
    const int initialShotsLeft = 15;
    int shotsLeft = initialShotsLeft;
//    float timeLeft = initialTimeLeft;
    bool startedSession = false;
    bool endedSession = false;
    [SerializeField] AudioSource beepAudioSource;

    public int displayIndex = 1;


    void Start()
    {
        for (int i = 1; i < 6; ++i)
        {
            if (Display.displays.Length > i)
            {
                Display.displays[i].Activate();
            }
        }

        //for (int i = 0; i < numAudioSources; ++i)
        //{
        //    GameObject g = Instantiate(bulletHitAudioSourceOrig);
        //    g.GetComponent<AudioSource>().clip = dirtImpactAudioClip; // default
        //    g.SetActive(true);
        //    bulletHitAudioSources[i] = g;
        //}

        //timeLeftValueText.text = shotsLeft.ToString();
    }


    private void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", 0.05f * Time.time);

        GetInput();
      
        //if (startedSession)
        //{
            //timeLeft -= Time.deltaTime;
            //if (timeLeft < 0)
            //{
            //    timeLeft = 0.0f;
            //    timeLeftText.text = "Session ended";
            //    timeLeftValueText.text = "";
            //    endedSession = true;
            //} else
            //{
            //    float coef = timeLeft / initialTimeLeft;
            //    timeIconBackground.fillAmount = coef;
            //    float r = 0.7f + 0.3f * (1.0f - coef);
            //    float g = 0.7f + 0.3f * coef;
            //    float b = 0.7f;
            //    float a = Mathf.Abs(Mathf.Cos(4.0f * Time.realtimeSinceStartup));
            //    timeIconBackground.color = new Color(r, g, b, a);
            //    timeLeftValueText.text = timeLeft.ToString("F2");
            //}
        //}
    }


    void GetInput()
    {
        if (Input.GetKeyUp(KeyCode.C))
        {
            Clear();
        }

        if (Input.GetKeyUp(KeyCode.Keypad0) || Input.GetKeyUp(KeyCode.Alpha0) || Input.GetKeyUp(KeyCode.Joystick1Button1))
        {
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.R) || Input.GetKeyUp(KeyCode.Joystick1Button2))
        {
            if (!startedSession)
            {
                startedSession = true;
                beepAudioSource.Play();
            } else
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }


    void Clear()
    {
        for (int i = 0; i < bulletHoles.Count; ++i)
        {
            Destroy(bulletHoles[i]);
        }
        bulletHoles.Clear();

        shots = 0;
        hits = 0;
        misses = 0;

        UpdateShotsTexts();
    }


    bool GetLaserPoint(ref Vector2 point) 
    {
        if (LaserDetectionClientTemp.pendingPosition)
        {
            Vector2 pos;
            pos = LaserDetectionClientTemp.laserPos;
            if (pos.x > 1.5f)
            {
                point.x = pos.x - 2;
                point.y = pos.y - 2;
            }
            else
            {
                //trace path 
                point = pos;
            }
            return true;
        }
        else 
        {
            return false;
        }
    }


    Vector2 GetUnityPos() 
    {
        Vector2 pos1=Vector2.zero;
        return pos1;
    }

    public void Shoot(Vector2 p)
    {
        Vector3 screenPos;
        screenPos.x = Display.displays[displayIndex].renderingWidth * p.x;
        screenPos.y = Display.displays[displayIndex].renderingHeight * p.y;
        //screenPos.x = 1600 * p.x;
        //screenPos.y = 1200 * p.y;
        screenPos.z = 0f;
        Ray ray = myCam.ScreenPointToRay(screenPos);
        //Ray ray = myCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        bool miss = true;

        if (Physics.Raycast(ray, out hit))
        {
            GameObject impactObj;

            if (hit.collider.gameObject.GetComponent<Terrain>() != null)
            {
                bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().clip = dirtImpactAudioClip;
                impactObj = Instantiate(dirtImpactOrig);
            }
            else
            {
                GameObject bulletHole = Instantiate(bulletHoleOrig);
                bulletHole.SetActive(true);
                bulletHole.transform.position = hit.point;
                //bulletHole.transform.parent = hit.collider.transform;
                bulletHoles.Add(bulletHole);

                //Targetable targ;
                //if (!hit.collider.transform.TryGetComponent<Targetable>(out targ))
                //{
                //    if (!hit.collider.transform.parent.TryGetComponent<Targetable>(out targ))
                //    {
                //        targ = null;
                //    }
                //}

                //if (targ != null)
                //{
                //    miss = false;

                //    switch (targ.material)
                //    {
                //        case Targetable.TargetMaterial.Wood:
                //            impactObj = Instantiate(woodImpactOrig);
                //            bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().clip = woodImpactAudioClip;
                //            break;
                //        case Targetable.TargetMaterial.Metal:
                //            impactObj = Instantiate(metalImpactOrig);
                //            bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().clip = metalImpactAudioClip;
                //            break;
                //        case Targetable.TargetMaterial.Dirt:
                //            impactObj = Instantiate(dirtImpactOrig);
                //            bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().clip = dirtImpactAudioClip;
                //            break;
                //        default:
                //            Debug.Log("Unknown target material, defaulting to dirt");
                //            bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().clip = dirtImpactAudioClip;
                //            impactObj = Instantiate(dirtImpactOrig);
                //            break;
                //    }

                //    if (targ.swingable)
                //    {
                //        Rigidbody rb = targ.gameObject.GetComponent<Rigidbody>();
                //        rb.AddForceAtPosition(new Vector3(0, 0, 30), new Vector3(0, -0.3f, 0), ForceMode.Impulse);
                //    }
                //}
                //else
                //{
                //    Debug.Log("Unknown non-target material, defaulting to dirt");
                //    bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().clip = dirtImpactAudioClip;
                //    impactObj = Instantiate(dirtImpactOrig);
                //}
            }

            //impactObj.transform.position = hit.point;
            //impactObj.GetComponent<ParticleSystem>().Play();
            //Destroy(impactObj, 3.0f);
            //StartCoroutine(DestroyImpactObject(impactObj, 2.0f));

            const float bulletSpeed = 50.0f; // m/s
            float timeDelay = /*hit.distance*/ 0.0f / bulletSpeed;
            bulletHitAudioSources[currentAudioSourceIndex].transform.position = hit.point;
            bulletHitAudioSources[currentAudioSourceIndex].GetComponent<AudioSource>().PlayDelayed(timeDelay);
            currentAudioSourceIndex = ++currentAudioSourceIndex % numAudioSources;
        }

        ++shots;
        if (miss)
        {
            ++misses;
        } else
        {
            ++hits;
        }

        UpdateShotsTexts();
    }


    void UpdateShotsTexts()
    {
        shotsValueText.text = shots.ToString();
        hitsValueText.text = hits.ToString();
        missesValueText.text = misses.ToString();
    }



    //IEnumerator DestroyImpactObject(GameObject g, float delay)
    //{
    //    yield return new WaitForSeconds(delay);
    //    Destroy(g);
    //}


    public void OnShotDetected(Vector2 p)
    {
        if (!startedSession || endedSession)
        {
            return;
        }

        Shoot(p);
        
        --shotsLeft;
        //if (shots < 0)
        //{
        //    shots = 0;
        //}

        if (shotsLeft == 0)
        {
            timeLeftText.text = "Out of rounds";
            timeLeftValueText.text = "";
            endedSession = true;
        } else
        {
            timeLeftText.text = "Rounds left";
            timeLeftValueText.text = shotsLeft.ToString();
        }

    }
}
