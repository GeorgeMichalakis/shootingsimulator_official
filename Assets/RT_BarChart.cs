using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
public class RT_BarChart : MonoBehaviour
{
    public GameObject references;
    public GameObject barOrig;
    public GameObject lineOrig;
    public GameObject xLabelOrig;
    public GameObject yLabelOrig;
    // public GameObject yMarkerOrig;
    public GameObject xLine;


    float properBarWidth;
    float barWidthMaxLimit = 20;
    float xLineLength; //xAxis length - zero x pos 

    Vector3 zero, zeroLoc;
    Vector3 xAxisMax, xAxisMaxLoc;
    Vector3 yAxisMax, yAxisMaxLoc;


    List<int> barElementValuesX = new List<int>();
    List<int> barElementValuesY = new List<int>();

    List<Entry> entries = new List<Entry>();

    public GameObject shotMarker;


    public enum GraphType
    {
        BarChart,
        LineChart
    }

    public GraphType graphType;

    //public LineRenderer lr;

    float barVertAnimationSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        xLineLength = xLine.GetComponent<RectTransform>().sizeDelta.x;
    }

    public void InitGraph(List<int> xElements, List<int> yElements)
    {
        barElementValuesX = xElements;
        barElementValuesY = yElements;
       
        FeedGraphData(barElementValuesX, barElementValuesY);
    }

    // Update is called once per frame
    void Update()
    {
        if (graphType == GraphType.BarChart)
        {
            AnimateBarsVertical();
        }

        //if (graphType == GraphType.LineChart) 
        {
           // AnimateLines();
        }
    }
    class Entry
    {
        public int amount;
        public GameObject bar;
        public GameObject line; // To next entry, if not being last
        public GameObject xLabel;
        public GameObject yLabel;
        public float posX;
        public float posY;
        public float nextPosX;
        public float nextPosY;
        //public GameObject yMarker;

        public float barFinalHeight;
        public float animProgress = 0; // 0.0f-1.0f;
    }

    void SetInitialData()
    {
        zero = references.transform.Find("Zero").position;
        xAxisMax = references.transform.Find("XAxisMax").position;
        yAxisMax = references.transform.Find("YAxisMax").position;

        zeroLoc = references.transform.Find("Zero").localPosition;
        xAxisMaxLoc = references.transform.Find("XAxisMax").localPosition;
        yAxisMaxLoc = references.transform.Find("YAxisMax").localPosition;
    }

   

    public void FeedGraphData(List<int> xs, List<int> ys)
    {
        SetInitialData();
        List<string> xLabels = new List<string>();
        Vector3 barOrigPos = barOrig.transform.position;
        Vector3 xLabelOrigPos = xLabelOrig.transform.position;
        Vector3 yLabelOrigPos = yLabelOrig.transform.position;
        //Vector3 yMarkerOrigPos = yMarkerOrig.transform.position;

        float xMin = xs.Min();
        float xMax = xs.Max();
        float yMax = ys.Max();
        float yMin = ys.Min();
        CalculateProperBarWidth(xs.Count);
        for (int i = 0; i < xs.Count; ++i)
        {
            Entry e = new Entry();
            GameObject b = Instantiate(barOrig);
            GameObject ln = Instantiate(lineOrig);
            GameObject xLab = Instantiate(xLabelOrig);
            GameObject yLab = Instantiate(yLabelOrig);
            //GameObject yMark = Instantiate(yMarkerOrig);

            if (xs.Min() == xs.Max())
            {
                // It's alone, there is no min-max scale, just put it in the middle
                e.posX = 0.5f * (zero.x + xAxisMax.x);
            }
            else
            {
                e.posX = zero.x + (xAxisMax.x - zero.x) * (xs[i] - xMin) / (xMax - xMin);
            }
            e.posY = zero.y + (yAxisMax.y - zero.y) * (ys[i]) / (yMax);
            e.nextPosX = (i < xs.Count - 1) ? zero.x + (xAxisMax.x - zero.x) * (xs[i + 1] - xMin) / (xMax - xMin) : -1f;
            e.nextPosY = (i < xs.Count - 1) ? zero.y + (yAxisMax.y - zero.y) * ys[i + 1] / yMax : -1f;

            // The bar
            b.transform.SetParent(barOrig.transform.parent);
            b.transform.position = new Vector3(e.posX, barOrigPos.y, barOrigPos.z);
            b.transform.rotation = Quaternion.identity;
            b.transform.localScale = Vector3.one;
            b.SetActive(true); // The default display mode
            b.name = xs[i].ToString();
            e.bar = b;

            // The line
            ln.transform.SetParent(lineOrig.transform.parent);
            ln.transform.position = new Vector3(e.posX, e.posY, lineOrig.transform.position.z);
            if (i == xs.Count - 1)
            {
                ln.transform.localScale = Vector3.zero;
            }
            else
            {
                ln.transform.localScale = Vector3.one;
            }
            ln.transform.rotation = Quaternion.Euler(0, 0, (float)Mathf.Atan2(e.nextPosY - e.posY, e.nextPosX - e.posX) * 180f / Mathf.PI);
            e.line = ln;

            // The X label
            xLab.transform.SetParent(xLabelOrig.transform.parent);
            xLab.transform.position = new Vector3(e.posX, xLabelOrigPos.y - 20, xLabelOrigPos.z);
            xLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            xLab.transform.localScale = Vector3.one;
            xLab.GetComponent<TMP_Text>().text = b.name;
            xLab.SetActive(true);
            e.xLabel = xLab;

            // The Y label
            e.barFinalHeight = (yAxisMaxLoc.y - zeroLoc.y) * ys[i] / yMax;
            yLab.transform.SetParent(yLabelOrig.transform.parent);
            //yLab.transform.position = new Vector3(e.posX, e.posY , yLabelOrigPos.z);
            yLab.transform.position = new Vector3(e.posX, e.posY + 50, yLabelOrigPos.z);
            yLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            yLab.transform.localScale = Vector3.one;
            //yLab.GetComponent<Text>().text = ys[i].ToString();
            yLab.SetActive(true);
            e.yLabel = yLab;
            e.amount = ys[i];

            entries.Add(e);
        }

    }
    public void FeedGraphData(List<float> xs, List<float> ys)
    {
        SetInitialData();
        List<string> xLabels = new List<string>();
        Vector3 barOrigPos = barOrig.transform.position;
        Vector3 xLabelOrigPos = xLabelOrig.transform.position;
        Vector3 yLabelOrigPos = yLabelOrig.transform.position;
        //Vector3 yMarkerOrigPos = yMarkerOrig.transform.position;

        float xMin = xs.Min();
        float xMax = xs.Max();
        float yMax = ys.Max();
        float yMin = ys.Min();
        CalculateProperBarWidth(xs.Count);
        for (int i = 0; i < xs.Count; ++i)
        {
            Entry e = new Entry();
            GameObject b = Instantiate(barOrig);
            GameObject ln = Instantiate(lineOrig);
            GameObject xLab = Instantiate(xLabelOrig);
            GameObject yLab = Instantiate(yLabelOrig);
            //GameObject yMark = Instantiate(yMarkerOrig);

            if (xs.Min() == xs.Max())
            {
                // It's alone, there is no min-max scale, just put it in the middle
                e.posX = 0.5f * (zero.x + xAxisMax.x);
            }
            else
            {
                e.posX = zero.x + (xAxisMax.x - zero.x) * (xs[i] - xMin) / (xMax - xMin);
            }
            e.posY = zero.y + (yAxisMax.y - zero.y) * (ys[i]) / (yMax);
            e.nextPosX = (i < xs.Count - 1) ? zero.x + (xAxisMax.x - zero.x) * (xs[i + 1] - xMin) / (xMax - xMin) : -1f;
            e.nextPosY = (i < xs.Count - 1) ? zero.y + (yAxisMax.y - zero.y) * ys[i + 1] / yMax : -1f;

            // The bar
            b.transform.SetParent(barOrig.transform.parent);
            b.transform.position = new Vector3(e.posX, barOrigPos.y, barOrigPos.z);
            b.transform.rotation = Quaternion.identity;
            b.transform.localScale = Vector3.one;
            b.SetActive(true); // The default display mode
            b.name = xs[i].ToString();
            e.bar = b;

            // The line
            ln.transform.SetParent(lineOrig.transform.parent);
            ln.transform.position = new Vector3(e.posX, e.posY, lineOrig.transform.position.z);
            if (i == xs.Count - 1)
            {
                ln.transform.localScale = Vector3.zero;
            }
            else
            {
                ln.transform.localScale = Vector3.one;
            }
            ln.transform.rotation = Quaternion.Euler(0, 0, (float)Mathf.Atan2(e.nextPosY - e.posY, e.nextPosX - e.posX) * 180f / Mathf.PI);
            e.line = ln;

            // The X label
            xLab.transform.SetParent(xLabelOrig.transform.parent);
            xLab.transform.position = new Vector3(e.posX, xLabelOrigPos.y - 20, xLabelOrigPos.z);
            xLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            xLab.transform.localScale = Vector3.one;
            xLab.GetComponent<TMP_Text>().text = b.name;
            xLab.SetActive(true);
            e.xLabel = xLab;

            // The Y label
            e.barFinalHeight = (yAxisMaxLoc.y - zeroLoc.y) * ys[i] / yMax;
            yLab.transform.SetParent(yLabelOrig.transform.parent);
            //yLab.transform.position = new Vector3(e.posX, e.posY , yLabelOrigPos.z);
            yLab.transform.position = new Vector3(e.posX, e.posY + 50, yLabelOrigPos.z);
            yLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            yLab.transform.localScale = Vector3.one;
            //yLab.GetComponent<Text>().text = ys[i].ToString();
            yLab.SetActive(true);
            e.yLabel = yLab;
            //e.amount = ys[i];

            entries.Add(e);
        }

    }

    public void FeedGraphData(List<int> xs, List<float> ys)
    {
        SetInitialData();
        List<string> xLabels = new List<string>();
        Vector3 barOrigPos = barOrig.transform.position;
        Vector3 xLabelOrigPos = xLabelOrig.transform.position;
        Vector3 yLabelOrigPos = yLabelOrig.transform.position;
        //Vector3 yMarkerOrigPos = yMarkerOrig.transform.position;

        int xMin = xs.Min();
        int xMax = xs.Max();
        float yMax = ys.Max();
        float yMin = ys.Min();
        CalculateProperBarWidth(xs.Count);
        for (int i = 0; i < xs.Count; ++i)
        {
            Entry e = new Entry();
            GameObject b = Instantiate(barOrig);
            GameObject ln = Instantiate(lineOrig);
            GameObject xLab = Instantiate(xLabelOrig);
            GameObject yLab = Instantiate(yLabelOrig);
            //GameObject yMark = Instantiate(yMarkerOrig);

            if (xs.Min() == xs.Max())
            {
                // It's alone, there is no min-max scale, just put it in the middle
                e.posX = 0.5f * (zero.x + xAxisMax.x);
            }
            else
            {
                e.posX = zero.x + (xAxisMax.x - zero.x) * (xs[i] - xMin) / (xMax - xMin);
            }
            e.posY = zero.y + (yAxisMax.y - zero.y) * (ys[i]) / (yMax);
            e.nextPosX = (i < xs.Count - 1) ? zero.x + (xAxisMax.x - zero.x) * (xs[i + 1] - xMin) / (xMax - xMin) : -1f;
            e.nextPosY = (i < xs.Count - 1) ? zero.y + (yAxisMax.y - zero.y) * ys[i + 1] / yMax : -1f;

            // The bar
            b.transform.SetParent(barOrig.transform.parent);
            b.transform.position = new Vector3(e.posX, barOrigPos.y, barOrigPos.z);
            b.transform.rotation = Quaternion.identity;
            b.transform.localScale = Vector3.one;
            b.SetActive(true); // The default display mode
            b.name = xs[i].ToString();
            e.bar = b;

            // The line
            ln.transform.SetParent(lineOrig.transform.parent);
            ln.transform.position = new Vector3(e.posX, e.posY, lineOrig.transform.position.z);
            if (i == xs.Count - 1)
            {
                ln.transform.localScale = Vector3.zero;
            }
            else
            {
                ln.transform.localScale = Vector3.one;
            }
            ln.transform.rotation = Quaternion.Euler(0, 0, (float)Mathf.Atan2(e.nextPosY - e.posY, e.nextPosX - e.posX) * 180f / Mathf.PI);
            e.line = ln;

            // The X label
            xLab.transform.SetParent(xLabelOrig.transform.parent);
            xLab.transform.position = new Vector3(e.posX, xLabelOrigPos.y - 20, xLabelOrigPos.z);
            xLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            xLab.transform.localScale = Vector3.one;
            xLab.GetComponent<TMP_Text>().text = b.name;
            xLab.SetActive(true);
            e.xLabel = xLab;

            // The Y label
            e.barFinalHeight = (yAxisMaxLoc.y - zeroLoc.y) * ys[i] / yMax;
            yLab.transform.SetParent(yLabelOrig.transform.parent);
            //yLab.transform.position = new Vector3(e.posX, e.posY , yLabelOrigPos.z);
            yLab.transform.position = new Vector3(e.posX, e.posY + 50, yLabelOrigPos.z);
            yLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
            yLab.transform.localScale = Vector3.one;
            //yLab.GetComponent<Text>().text = ys[i].ToString();
            yLab.SetActive(true);
            e.yLabel = yLab;
            //e.amount = ys[i];

            entries.Add(e);
        }

    }
    public LineRenderer testLr;
    public void FeedLineGraphDataNoLabels(List<float> xs, List<float> ys,float timeShot)
    {
        shotMarker.SetActive(false);
        SetInitialData();
        Vector3 xLabelOrigPos = xLabelOrig.transform.position;

        float xMin = xs.Min();
        float xMax = xs.Max();
        float yMax = ys.Max();
        float yMin = ys.Min();
        testLr.positionCount = xs.Count;
        CalculateProperBarWidth(xs.Count);
        for (int i = 0; i < xs.Count; ++i)
        {
            Entry e = new Entry();

            //GameObject ln = Instantiate(lineOrig);

            //GameObject yMark = Instantiate(yMarkerOrig);


            if (xs.Min() == xs.Max())
            {
                // It's alone, there is no min-max scale, just put it in the middle
                e.posX = 0.5f * (zero.x + xAxisMax.x);
            }
            else
            {
                e.posX = zero.x + (xAxisMax.x - zero.x) * (xs[i] - xMin) / (xMax - xMin);
            }
            e.posY = zero.y + (yAxisMax.y - zero.y) * (ys[i] - yMin) / (yMax - yMin);
            //e.posY = zero.y + (yAxisMax.y - zero.y) * (ys[i]) / (yMax);
            e.nextPosX = (i < xs.Count - 1) ? zero.x + (xAxisMax.x - zero.x) * (xs[i + 1] - xMin) / (xMax - xMin) : -1f;
            e.nextPosY = (i < xs.Count - 1) ? zero.y + (yAxisMax.y - zero.y) *( ys[i + 1] - yMin) /( yMax - yMin) : -1f;

            if (xs[i] == -1.0f || xs[i] == 0.0f || xs[i] == -0.02f || xs[i] == 1.0f || xs[i] == -2.0f) 
            {
                Debug.Log("FOUND ONE");
                GameObject xLab = Instantiate(xLabelOrig);
                xLab.transform.SetParent(xLabelOrig.transform.parent);
                xLab.transform.position = new Vector3(e.posX, xLabelOrigPos.y - 20, xLabelOrigPos.z);
                xLab.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                xLab.transform.localScale = Vector3.one;
                xLab.GetComponent<TMP_Text>().text = xs[i].ToString();
                xLab.SetActive(true);
                e.xLabel = xLab;
            }

            if (xs[i] == timeShot) 
            {
                shotMarker.transform.position = new Vector3(e.posX, e.posY, transform.position.z - 2);
                shotMarker.SetActive(true);
            }


            // The line
            //ln.transform.SetParent(lineOrig.transform.parent);
            //ln.transform.position = new Vector3(e.posX, e.posY, lineOrig.transform.position.z);
            //if (i == xs.Count - 1)
            //{
            //    ln.transform.localScale = Vector3.zero;
            //}
            //else
            //{
            //    ln.transform.localScale = Vector3.one;
            //}
            //ln.transform.rotation = Quaternion.Euler(0, 0, (float)Mathf.Atan2(e.nextPosY - e.posY, e.nextPosX - e.posX) * 180f / Mathf.PI);
            //e.line = ln;
            //e.amount = ys[i];

            testLr.SetPosition(i, new Vector3(e.posX, e.posY, transform.position.z-1));

            entries.Add(e);
        }

    }
    public void FeedLineGraphDataNoLabels(List<int> xs, List<float> ys)
    {
        SetInitialData();
        List<string> xLabels = new List<string>();
        Vector3 barOrigPos = barOrig.transform.position;
        Vector3 xLabelOrigPos = xLabelOrig.transform.position;
        Vector3 yLabelOrigPos = yLabelOrig.transform.position;
        //Vector3 yMarkerOrigPos = yMarkerOrig.transform.position;

        int xMin = xs.Min();
        int xMax = xs.Max();
        float yMax = ys.Max();
        float yMin = ys.Min();
        CalculateProperBarWidth(xs.Count);
        for (int i = 0; i < xs.Count; ++i)
        {
            Entry e = new Entry();

           // GameObject ln = Instantiate(lineOrig);

            //GameObject yMark = Instantiate(yMarkerOrig);

            if (xs.Min() == xs.Max())
            {
                // It's alone, there is no min-max scale, just put it in the middle
                e.posX = 0.5f * (zero.x + xAxisMax.x);
            }
            else
            {
                e.posX = zero.x + (xAxisMax.x - zero.x) * (xs[i] - xMin) / (xMax - xMin);
            }
            e.posY = zero.y + (yAxisMax.y - zero.y) * (ys[i]) / (yMax);
            e.nextPosX = (i < xs.Count - 1) ? zero.x + (xAxisMax.x - zero.x) * (xs[i + 1] - xMin) / (xMax - xMin) : -1f;
            e.nextPosY = (i < xs.Count - 1) ? zero.y + (yAxisMax.y - zero.y) * ys[i + 1] / yMax : -1f;

            // The line
            //ln.transform.SetParent(lineOrig.transform.parent);
            //ln.transform.position = new Vector3(e.posX, e.posY, lineOrig.transform.position.z);
            //if (i == xs.Count - 1)
            //{
            //    ln.transform.localScale = Vector3.zero;
            //}
            //else
            //{
            //    ln.transform.localScale = Vector3.one;
            //}
            //ln.transform.rotation = Quaternion.Euler(0, 0, (float)Mathf.Atan2(e.nextPosY - e.posY, e.nextPosX - e.posX) * 180f / Mathf.PI);
            //e.line = ln;
            //e.amount = ys[i];

            entries.Add(e);
        }

    }




    void CalculateProperBarWidth(int elementCount)
    {
        properBarWidth = xLineLength / (elementCount);
        Debug.Log($"split  {elementCount}  elements, with  {properBarWidth} length");
        //float padding = (properBarWidth / 2)/300;
        //float offset = (properBarWidth * padding) / 100;
        properBarWidth = (properBarWidth / 2);
        Debug.Log("Proper bar width" + properBarWidth.ToString());
        if (properBarWidth > barWidthMaxLimit)
        {
            properBarWidth = barWidthMaxLimit;
        }
    }


    void AnimateBarsVertical()
    {
        for (int i = 0; i < entries.Count; ++i)
        {
            float diff = (1f - entries[i].animProgress);
            float coef = diff > 0 ? Mathf.Sqrt(diff) : -Mathf.Sqrt(-diff);

            entries[i].animProgress += barVertAnimationSpeed * Time.deltaTime * coef;
            if (entries[i].animProgress > 0.99999f)
            {
                entries[i].animProgress = 1f;
            }

            //float barWidth = entries[i].bar.GetComponent<RectTransform>().sizeDelta.x;
            float targetHeight = entries[i].barFinalHeight;

            entries[i].bar.GetComponent<RectTransform>().sizeDelta = new Vector2(properBarWidth, entries[i].animProgress * targetHeight);

            int currAmount = (int)(entries[i].animProgress * entries[i].amount);
            entries[i].yLabel.GetComponent<TMP_Text>().text = currAmount.ToString();
        }
    }


    void AnimateLines()
    {
        for (int i = 0; i < entries.Count - 1; ++i)
        {
            float p1x = entries[i].yLabel.GetComponent<RectTransform>().anchoredPosition.x;
            float p1y = entries[i].yLabel.GetComponent<RectTransform>().anchoredPosition.y;
            float p2x = entries[i + 1].yLabel.GetComponent<RectTransform>().anchoredPosition.x;
            float p2y = entries[i + 1].yLabel.GetComponent<RectTransform>().anchoredPosition.y;

            float dis = Mathf.Sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y));

            Vector2 currSize = entries[i].line.GetComponent<RectTransform>().sizeDelta;

            entries[i].line.GetComponent<RectTransform>().sizeDelta = new Vector2(entries[i].animProgress * dis, currSize.y);
        }
    }


    //void SetLineRenderer()
    //{
    //    if (lr != null) 
    //    {
    //        lr.positionCount = entries.Count;
    //        for (int i = 0; i < lr.positionCount; i++)
    //        {
    //            Vector3 pointPos = new Vector3(entries[i].posX , entries[i].posY , -1);
    //           //Debug.Log("point " + pointPos.ToString());
    //            Vector3 worldPointPos = lr.transform.TransformPoint(pointPos);
    //            //Vector3 pointPos = new Vector3(entries[i].posX + transform.position.x, entries[i].posY + transform.position.y, 0);
    //            lr.SetPosition(i, worldPointPos);

    //        }


    //    }
    //}

    public void DisableLr() 
    {
        testLr.positionCount = 0;
    }

}
