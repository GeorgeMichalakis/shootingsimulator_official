using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class RT_Engagements : MonoBehaviour
{
    ScenarioManager tempManager;



    public GameObject shotTracePanel;
    RT_ShotCalculator shotTracePanelScript;
    bool trainningInProgress = false;

    public List<GameObject> targetGosE1 = new List<GameObject>();
    public List<GameObject> targetGosE2 = new List<GameObject>();
    public List<GameObject> targetGosE3 = new List<GameObject>();
    public List<RT_ShotTrace> shotTracePanels = new List<RT_ShotTrace>();

    int targetIndex = -1;
    int engagementIndex = 0;
    bool engagementEnded = false;
    bool isAllowedToShoot = false;
    float timeToShootTarget = 0;
    float totalTimeOfEngagement = 0;

    float timeTargetWasShot = 0;

    int shotsFiredInEngagement = 0;
    int numberOfSuccessfulShots = 0;

    private bool nextTargetReady = false;
    private bool once = false;
    public enum EnagagementSeries
    {
        OneToNine,
        TenToSeventeen,
        EighteentoTwentythree
    }


    [SerializeField] GameObject shotDummy;
    List<float> currentEngagementsScores = new List<float>();
    List<int> currentEngagementIndexes = new List<int>();
    List<float> currentEngagementTimes = new List<float>();

    public RT_BarChart accuracyBar;
    public RT_BarChart timeBar;
    //engagement stats


    public List<RT_EngagementStats> engagementStatsList = new List<RT_EngagementStats>();


    // Start is called before the first frame update
    void Start()
    {
        tempManager = GetComponentInParent<ScenarioManager>();
        Init();

    }

    // Update is called once per frame
    void Update()
    {


        if (!trainningInProgress) { return; }
        UpdateTimer();
        if (isTimerOn)
        {
            ShotCalculation(GetTargetGoForEngagment(engagementIndex, targetIndex), GetTargetImageGoForEngagment(targetIndex), GetShotMarkerImageGoForEngagment(targetIndex));
        }
        ActivateNextEngagement();
    }


    void Init()
    {

        shotTracePanelScript = shotTracePanel.GetComponent<RT_ShotCalculator>();
        for (int i = 0; i < shotTracePanels.Count; i++)
        {
            //targetGosE1[i].transform.eulerAngles = new Vector3(-90, 0, 0);
            //targetGosE2[i].transform.eulerAngles = new Vector3(-90, 0, 0);
            //targetGosE3[i].transform.eulerAngles = new Vector3(-90, 0, 0);
            shotTracePanels[i].GetComponent<Image>().color = Color.red;
            //shotTracePanels[i].bg.color = Color.red;

        }
    }


    #region Buttons
    //Run buttons
    public void ExecuteBtn()
    {
        if (trainningInProgress) { return; }
        tempManager.executeDrill = true;
        trainningInProgress = true;
        engagementEnded = false;
        ActivateNextEngagementTarget();
    }
    public void PauseBtn()
    {
        trainningInProgress = !trainningInProgress;
    }

    public void StopBtn()
    {
        trainningInProgress = false;
    }

    public void ResetBtn()
    {
        //restart scene?

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    #endregion

    void SetAccuracyBar()
    {
        accuracyBar.InitGraph(GenerateRandSet(10, 10), GenerateRandSet(10, 20));
    }
    void SetTimeBar()
    {
        timeBar.InitGraph(GenerateRandSet(15, 20), GenerateRandSet(15, 20));
    }


    List<int> GenerateRandSet(int elementCount, int maxValue)
    {
        List<int> barElementValues = new List<int>();
        for (int i = 0; i < elementCount; i++)
        {
            int randx = UnityEngine.Random.Range(0, maxValue);
            while (barElementValues.Contains(randx))
            {
                randx = UnityEngine.Random.Range(0, maxValue);
            }
            barElementValues.Add(randx);
        }
        return barElementValues;
    }


    void ActivateNextEngagementTarget()
    {
        //GetPointsBeforeAndAfterShot(timeTargetWasShot);
        if (!nextTargetReady)
        {
            StartCoroutine(WaitTimeBetweenTargets());
            return;
        }
        else
        {
            if (targetIndex > 0 && targetIndex < shotTracePanels.Count)
            {
                if (shotTracePanels[targetIndex].shotMarker.enabled == true)
                {
                    shotTracePanels[targetIndex].shotMarker.enabled = false;
                }
            }
            totalTimeOfEngagement += timeToShootTarget;
            if (targetIndex >= 0)
            {
                GetPointListArrayForEngagement(engagementIndex)[targetIndex] = new List<ShotTracePoint>(rayPoints);

                //Debug.Log($"target {targetIndex} had {rayPoints.Count} raypoints, transfered { GetPointListArrayForEngagement(engagementIndex)[targetIndex].Count}");
            }
            rayPoints.Clear();
            //allaRayData[targetIndex] = rayPoints;
            targetIndex++;
            timeToShootTarget = 0;


            if (targetIndex > shotTracePanels.Count - 1)
            {
                CalculateEngagementAccuracy();
                engagementEnded = true;
                return;
                //activate next engagement         
            }

            if (shotTracePanels[targetIndex].shotMarker.enabled == false)
            {
                shotTracePanels[targetIndex].shotMarker.enabled = true;
            }

            isAllowedToShoot = true;

            GameObject targetOfEngagement = GetTargetGoForEngagment(engagementIndex, targetIndex);
            //targetOfEngagement.SetActive(true);
            //targetOfEngagement.transform.eulerAngles = new Vector3(0, 0, 0);
            //targetGos[targetIndex].SetActive(true);
            shotTracePanels[targetIndex].GetComponent<Image>().color = Color.green;
            StartTimer(tempManager.time[(targetIndex) * (engagementIndex)]);
            //StartTimer(3.5f);
        }

    }

    public void ActivateNextEngagement()
    {
        if (engagementEnded)
        {
            //DisableTargetsOfEngagementIndex(engagementIndex);
            totalTimeOfEngagement = 0;
            shotsFiredInEngagement = 0;
            numberOfSuccessfulShots = 0;
            engagementIndex++;
            if (engagementIndex <= 2)
            {
                targetIndex = -1;
                for (int i = 0; i < shotTracePanels.Count; i++)
                {
                    shotTracePanels[i].GetComponent<Image>().color = Color.red;
                    shotTracePanels[i].targetShotImage.enabled = false;
                    shotTracePanels[i].shotMarker.enabled = false;
                }

                nextTargetReady = false;
                StartCoroutine(MagazineChange());
            }
        }
    }

    #region Timer
    float engagementMaxTimer;

    float engagementTimer = 0;

    bool isTimerOn = false;

    void StartTimer(float timeToRun)
    {
        engagementMaxTimer = timeToRun;
        isTimerOn = true;
    }

    void Stoptimer()
    {
        isTimerOn = false;
        engagementTimer = 0;
    }

    void UpdateTimer()
    {
        if (isTimerOn)
        {
            engagementTimer += Time.deltaTime;
            timeToShootTarget += Time.deltaTime;
            if (engagementTimer > engagementMaxTimer)
            {
                Stoptimer();
                nextTargetReady = false;
                ActivateNextEngagementTarget();
                //calculate previous engagement stats and addd it to the stats panel
                //add time and accuracy in each loop for the previous shots 
            }
        }
    }

    #endregion


    #region Targets
    //Gos
    GameObject GetTargetGoForEngagment(int engagementIndex, int targetIndex)
    {
        List<GameObject> targetList = new List<GameObject>();
        switch (engagementIndex)
        {
            case 0:
                targetList = targetGosE1;
                break;
            case 1:
                targetList = targetGosE2;
                break;
            case 2:
                targetList = targetGosE3;
                break;
        }

        return tempManager.GetTargetForEngagement(targetIndex);

        //return targetList[targetIndex];
    }

    //Ui 
    Image GetTargetImageGoForEngagment(int targetIndex)
    {
        return shotTracePanels[targetIndex].targetImage;
    }

    Image GetShotMarkerImageGoForEngagment(int targetIndex)
    {
        return shotTracePanels[targetIndex].shotMarker;
    }

    #endregion


    #region Shot calculation

    void ShotCalculation(GameObject target, Image targetImage, Image shotUI)
    {
        //if (isTargetActive)
        { // if left button pressed...
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                // the object identified by hit.transform was clicked
                // do whatever you want
                if (hit.collider.gameObject == target)
                {
                    BoxCollider targetCol = target.GetComponent<BoxCollider>();
                    float worldTargetGoWidth = targetCol.size.x;
                    float worldTargetGoHeight = targetCol.size.y;

                    shotDummy.transform.position = hit.point;


                    Vector3 shotLocalPos = target.transform.InverseTransformPoint(shotDummy.transform.position);
                    //shotLocalPos += targetCol.center;

                    //float localPctWidth = ((worldTargetGoWidth * 100) - ((worldTargetGoWidth - shotLocalPos.x) * 100)) / 100;
                    //float localPctHeight = ((worldTargetGoHeight * 100) - ((worldTargetGoHeight - shotLocalPos.y) * 100)) / 100;

                    float localPctWidth = (shotLocalPos.x) / worldTargetGoWidth;
                    float localPctHeight = (shotLocalPos.y) / worldTargetGoHeight;

                    float widthFactor = targetImage.GetComponent<RectTransform>().sizeDelta.x * localPctWidth;

                    float heightFactor = targetImage.GetComponent<RectTransform>().sizeDelta.y * localPctHeight;

                    Debug.Log($"shot normal pos {hit.point} shot localPos {shotLocalPos} local pct x is : {localPctWidth},total is {worldTargetGoWidth} , and y is : {localPctHeight} , total is {worldTargetGoHeight}, width max is {targetImage.GetComponent<RectTransform>().sizeDelta.x} / {localPctWidth} = { widthFactor}, height max is {targetImage.GetComponent<RectTransform>().sizeDelta.y} / {localPctHeight} = { heightFactor}");


                    shotUI.transform.localPosition = new Vector3(widthFactor, heightFactor, 0);

                    Vector3 linePoint = targetImage.transform.TransformPoint(shotUI.transform.localPosition);

                    if (shotTracePanels[targetIndex].shotMarker.enabled == false)
                    {
                        shotTracePanels[targetIndex].shotMarker.enabled = true;
                    }

                    // linePoints.Add(linePoint);
                    //Debug.Log($" sot world is : {hit.point.ToString()} and transfered to : {shotUI.transform.localPosition.ToString()} , x factor is : {widthFactor.ToString()} and y factor is : {heightFactor.ToString()}");
                    float xdeflation = widthFactor;
                    float ydeflation = heightFactor;

                    ShotTracePoint point = new ShotTracePoint();

                    point.time = timeToShootTarget;
                    //point.time = Time.deltaTime;

                    point.targetGoPoint = shotLocalPos;
                    point.targetUiPoint = linePoint;



                    point.xDeflation = xdeflation;
                    point.yDeflation = ydeflation;

                    point.pctPositionOnTarget = new Vector3(localPctWidth, localPctHeight, 0);

                    if (Input.GetMouseButtonDown(0) && isAllowedToShoot)
                    {
                        shotTracePanels[targetIndex].targetShotImage.transform.localPosition = new Vector3(widthFactor, heightFactor, 0);
                        shotTracePanels[targetIndex].targetShotImage.enabled = true;
                        point.bulletWasShot = true;
                        shotTracePanels[targetIndex].shotTraceBtn.interactable = true;

                        var ts = TimeSpan.FromSeconds(timeTargetWasShot);
                        shotTracePanels[targetIndex].executionTimeText.text = string.Format("{00:00}:{01:00}", ts.Minutes, ts.Seconds, ts.Milliseconds);

                        //shotTracePanels[targetIndex].executionTimeText.text = timeTargetWasShot.ToString("");

                        CalculateAccuracyOfShot(engagementIndex, targetIndex);
                        isAllowedToShoot = false;
                    }
                    rayPoints.Add(point);

                }
                else
                {
                    if (shotTracePanels[targetIndex].shotMarker.enabled == true)
                    {
                        shotTracePanels[targetIndex].shotMarker.enabled = false;
                    }
                }
            }

            //shotPathlr.positionCount = linePoints.Count;
            //for (int i = 0; i < linePoints.Count; i++)
            //{
            //    shotPathlr.SetPosition(i, linePoints[i]);
            //}
        }
    }

    //3 is number of engagements 
    List<int>[] engagementShotPoints = new List<int>[3];

    void CalculateAccuracyOfShot(int engagementIndex, int targetIndex)
    {
        shotsFiredInEngagement++;
        timeTargetWasShot = timeToShootTarget;

        float accuracyPoints = Vector3.Distance(shotTracePanels[targetIndex].shotMarker.transform.position, shotTracePanels[targetIndex].targetCenter.position);
        currentEngagementsScores.Add(accuracyPoints);
        currentEngagementIndexes.Add(targetIndex + 1);

        Debug.Log($"Accuracy {accuracyPoints} % for target {targetIndex + 1}");
        currentEngagementTimes.Add(timeToShootTarget);

        shotTracePanels[targetIndex].timeBulletWasShot = timeTargetWasShot;




        if (accuracyPoints < 2f)
        {
            numberOfSuccessfulShots++;
        }
    }

    void CalculateEngagementAccuracy()
    {
        accuracyBar.FeedGraphData(currentEngagementIndexes, currentEngagementsScores);
        timeBar.FeedGraphData(currentEngagementIndexes, currentEngagementTimes);

        //stats
        float successRate = (float)numberOfSuccessfulShots / (float)shotsFiredInEngagement;
        successRate *= 100.0f;
        Debug.Log($"engagement successrate is {successRate}, number of ok shots   {numberOfSuccessfulShots} / {shotsFiredInEngagement}");
        engagementStatsList[engagementIndex].SetEngagementStats(successRate, totalTimeOfEngagement, shotsFiredInEngagement);
    }

    void Shoot()
    {
    }
    void DisableTargetsOfEngagementIndex(int engagementIndex)
    {
        for (int i = 0; i < shotTracePanels.Count; i++)
        {
            GetTargetGoForEngagment(engagementIndex, i).SetActive(false);
        }

    }


    #endregion


    #region Shot trace data 

    [System.Serializable]
    public class ShotTracePoint
    {
        public float time;
        public Vector3 targetGoPoint;
        public Vector3 targetUiPoint;
        public float speed;
        public float xDeflation;
        public float yDeflation;

        public Vector3 pctPositionOnTarget;
        public bool bulletWasShot = false;
    }

    List<ShotTracePoint> rayPoints = new List<ShotTracePoint>();
    [SerializeField]
    public List<ShotTracePoint>[] allaRayDataE1 = new List<ShotTracePoint>[10];
    List<ShotTracePoint>[] allaRayDataE2 = new List<ShotTracePoint>[10];
    List<ShotTracePoint>[] allaRayDataE3 = new List<ShotTracePoint>[10];

    List<ShotTracePoint>[] GetPointListArrayForEngagement(int engagementIndex)
    {
        List<ShotTracePoint>[] targetList = new List<ShotTracePoint>[10];
        switch (engagementIndex)
        {
            case 0:
                targetList = allaRayDataE1;
                break;
            case 1:
                targetList = allaRayDataE2;
                break;
            case 2:
                targetList = allaRayDataE3;
                break;
        }
        return targetList;
    }
    public void EnableShotTraceForTarget(int targetIndex)
    {
        shotTracePanel.SetActive(true);
        shotTracePanelScript.SetAimTrail(GetPointListArrayForEngagement(engagementIndex)[targetIndex], shotTracePanels[targetIndex].timeBulletWasShot);
        Debug.Log($"activating panel of target {targetIndex} and engagement Index {engagementIndex}, stored points count is {GetPointListArrayForEngagement(engagementIndex)[targetIndex].Count}");
    }


    void GetPointsBeforeAndAfterShot(float time)
    {
        for (int i = 0; i < rayPoints.Count; i++)
        {
            float timerDif = rayPoints[i].time - time;
            if (!IsBetweenRange(timerDif, -1.5f, 1.0f))
            {
                rayPoints.Remove(rayPoints[i]);
            }
        }
    }

    public bool IsBetweenRange(float thisValue, float value1, float value2)
    {
        return thisValue >= Mathf.Min(value1, value2) && thisValue <= Mathf.Max(value1, value2);
    }

    #endregion


    void OnLaserDetected(Vector2 p)
    {
        Debug.Log("Laser event");
        if (p.x > 1.5f)
        {
            //shot
        }
        else
        {
            //trace on screen

        }
    }

    IEnumerator WaitTimeBetweenTargets()
    {
        yield return new WaitForSeconds(2f);
        nextTargetReady = true;
        ActivateNextEngagementTarget();
    }

    IEnumerator MagazineChange()
    {
        yield return new WaitForSeconds(8f);
        nextTargetReady = true;
        ActivateNextEngagementTarget();
    }
}
