using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RT_ShotCalculator : MonoBehaviour
{

    public GameObject target;
    public GameObject shot;

    public Image targetImage;
    public Image targetDeflationImage;
    public GameObject shotUI;
    public GameObject shotUiDeflation;

    public Transform deflationImageCenter;
    public Image deflationBoxImage;

    RectTransform uiRect;


    bool isTargetActive = false;

    Vector3 centerOfTarget;

    List<Vector3> linePoints = new List<Vector3>();

    [SerializeField] RT_BarChart xDeflationChart;
    [SerializeField] RT_BarChart yDeflationChart;
    [SerializeField] RT_BarChart traceSpeedChart;


    //Stats
    List<float> xDeflationList = new List<float>();
    List<float> yDeflationList = new List<float>();
    List<float> traceSpeedList = new List<float>();
    List<float> timeSteps = new List<float>();

    Vector3 previousPosition;

    //ui dimensions

    float targetImageWidth;

    float targetImageHeight;
    [SerializeField] LineRenderer shotPathlr;
    [SerializeField] LineRenderer shotTrailLr1;
    List<Vector3> trailsPoints1 = new List<Vector3>();
    [SerializeField] LineRenderer shotTrailLr2;
    List<Vector3> trailsPoints2 = new List<Vector3>();
    [SerializeField] LineRenderer shotTrailLr3;
    List<Vector3> trailsPoints3 = new List<Vector3>();
    [SerializeField] LineRenderer shotTrailLr4;
    List<Vector3> trailsPoints4 = new List<Vector3>();

    public class FireArmRayPoint
    {
        public float time;
        public Vector3 targetGoPoint;
        public Vector3 targetUiPoint;
        public float speed;
        public float xDeflation;
        public float yDeflation;

    }

    List<FireArmRayPoint> rayPoints = new List<FireArmRayPoint>();


    // Start is called before the first frame update
    void Start()
    {
        uiRect = targetImage.GetComponent<RectTransform>();
        //uiOffset = new Vector2((float)uiRect.sizeDelta.x / 2f, (float)uiRect.sizeDelta.y / 2f);
        centerOfTarget = target.transform.position;

        shotUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //ShotCalculation();

        //if (Input.GetMouseButtonDown(0))
        //{
        //    isTargetActive = true;
        //}
        //if (Input.GetMouseButtonUp(0))
        //{
        //    isTargetActive = false;

        //    List<int> xValues = new List<int>();
        //    List<int> yValues = new List<int>();
        //    for (int i = 0; i < rayPoints.Count; i++)
        //    {
        //        xValues.Add((int)rayPoints[i].xDeflation);
        //        yValues.Add(i);
        //    }


        //    xDeflationChart.InitGraph(xValues, yValues);
        //}
    }


    public void ReturnButton()
    {
        //close all panels
        ResetAllTrailPaths();
        shotUI.SetActive(false);
        traceSpeedChart.DisableLr();
        xDeflationChart.DisableLr();
        yDeflationChart.DisableLr();

        gameObject.SetActive(false);
    }



    public void SetAimTrail(List<RT_Engagements.ShotTracePoint> points, float timeShot)
    {
        traceSpeedList.Clear();
        xDeflationList.Clear();
        yDeflationList.Clear();
        timeSteps.Clear();

        targetImageWidth = targetImage.GetComponent<RectTransform>().sizeDelta.x;

        targetImageHeight = targetImage.GetComponent<RectTransform>().sizeDelta.y;

        if (points == null) { return; }
        shotPathlr.positionCount = points.Count;
        for (int i = 0; i < points.Count; i++)
        {


            float widthFactor = targetImageWidth * points[i].pctPositionOnTarget.x;
            //Debug.Log($"pos x is  WF {widthFactor}, image width is {targetImageWidth} , and pct of width is {points[i].pctPositionOnTarget} ");

            float heightFactor = targetImageHeight * points[i].pctPositionOnTarget.y;
            //relative pos minus center of image to avoid missmatch due to pivot points

            Vector3 pointUiPos = new Vector3(widthFactor, heightFactor, 0);// - targetImage.transform.GetChild(0).position;

            Vector3 linePointTraceShot = targetImage.transform.TransformPoint(pointUiPos);
            Vector3 linePointDeflationShot = targetDeflationImage.transform.TransformPoint(pointUiPos);

            float timeValue = points[i].time - timeShot;
            if (IsBetweenRange(timeValue, -2.0f, -1.0f))
            {
                trailsPoints1.Add(linePointTraceShot);
            }
            else if (IsBetweenRange(timeValue, -1.0f, -0.02f))
            {
                trailsPoints2.Add(linePointTraceShot);
            }
            else if (IsBetweenRange(timeValue, -0.2f, timeShot))
            {
                trailsPoints3.Add(linePointTraceShot);

            }
            else if (IsBetweenRange(timeValue, timeShot, 2.0f))
            {
                trailsPoints4.Add(linePointTraceShot);

            }
            else { continue; }

            if (points[i].bulletWasShot)
            {
                shotUI.SetActive(true);
                shotUI.transform.position = linePointTraceShot;
                shotUiDeflation.transform.position = linePointDeflationShot;
                //Debug.Log($"Placing marker on bullet pos-> pct is  {points[i].pctPositionOnTarget.ToString()} to {linePoint}");

                SetDeflationBox(widthFactor, heightFactor);
                SetShotStatsPanel("UL", points[i].xDeflation, points[i].yDeflation, points[i].time, "Success");
            }

            // shotPathlr.SetPosition(i, linePoint);
            xDeflationList.Add(points[i].xDeflation);
            yDeflationList.Add(points[i].yDeflation);
            timeSteps.Add(points[i].time);
            traceSpeedList.Add(GetPointSpeed(points[i].targetGoPoint, timeSteps[i]));
        }
        SetAllTrailPaths();
        traceSpeedChart.FeedLineGraphDataNoLabels(timeSteps, traceSpeedList, timeShot);
        xDeflationChart.FeedLineGraphDataNoLabels(timeSteps, xDeflationList, timeShot);
        yDeflationChart.FeedLineGraphDataNoLabels(timeSteps, yDeflationList, timeShot);
    }

    float GetPointSpeed(Vector3 currentPosition, float time)
    {
        float velocity = ((currentPosition - previousPosition).magnitude) / Time.deltaTime;
        //float velocity = ((currentPosition - previousPosition).magnitude) / time;
        previousPosition = currentPosition;
        return velocity;
    }

    void SetDeflationBox(float x, float y)
    {

        float boxScaleFactorX = targetDeflationImage.rectTransform.sizeDelta.x / deflationBoxImage.rectTransform.sizeDelta.x;
        float boxScaleFactorY = targetDeflationImage.rectTransform.sizeDelta.y / deflationBoxImage.rectTransform.sizeDelta.y;

        Vector3 fromCenterToHit = shotUiDeflation.transform.localPosition - deflationImageCenter.localPosition;
        float distance = fromCenterToHit.magnitude;
        Vector3 fromCenterToHitInitial = fromCenterToHit;
        fromCenterToHit = new Vector3(-fromCenterToHit.x / 2, fromCenterToHit.y / 2, deflationImageCenter.position.z);


        Vector2 DefLength = new Vector2(fromCenterToHitInitial.x, fromCenterToHitInitial.y);
        Vector2 xDefLengthProjection = new Vector2(DefLength.x, 0);
        Vector2 yDefLengthProjection = new Vector2(0, DefLength.y);

        deflationBoxImage.transform.localPosition = Vector3.Lerp(shotUiDeflation.transform.localPosition, deflationImageCenter.transform.localPosition, 0.5f);
        //deflationBoxImage.transform.localPosition = fromCenterToHit;

        deflationBoxImage.rectTransform.sizeDelta = new Vector2(Mathf.Abs(xDefLengthProjection.x), Mathf.Abs(yDefLengthProjection.y));
        Debug.Log($"Def box  | center to hit Length  { fromCenterToHitInitial.magnitude.ToString()} x langth proj  = {xDefLengthProjection.x} , y = {yDefLengthProjection.y}");

        Debug.Log($" Box : XProje Length {xDefLengthProjection.x} , y proj length {yDefLengthProjection.y} , distance between points {distance.ToString()}");
    }

    void SetAllTrailPaths()
    {
        shotTrailLr1.positionCount = trailsPoints1.Count;
        for (int i = 0; i < trailsPoints1.Count; i++)
        {
            shotTrailLr1.SetPosition(i, trailsPoints1[i]);
        }
        shotTrailLr2.positionCount = trailsPoints2.Count;
        for (int i = 0; i < trailsPoints2.Count; i++)
        {
            shotTrailLr2.SetPosition(i, trailsPoints2[i]);
        }
        shotTrailLr3.positionCount = trailsPoints3.Count;
        for (int i = 0; i < trailsPoints3.Count; i++)
        {
            shotTrailLr3.SetPosition(i, trailsPoints3[i]);
        }
        shotTrailLr4.positionCount = trailsPoints4.Count;
        for (int i = 0; i < trailsPoints4.Count; i++)
        {
            shotTrailLr4.SetPosition(i, trailsPoints4[i]);
        }
    }

    void ResetAllTrailPaths()
    {
        shotTrailLr1.positionCount = 0;
        trailsPoints1.Clear();
        shotTrailLr2.positionCount = 0;
        trailsPoints2.Clear();
        shotTrailLr3.positionCount = 0;
        trailsPoints3.Clear();
        shotTrailLr4.positionCount = 0;
        trailsPoints4.Clear();

    }

    public bool IsBetweenRange(float thisValue, float value1, float value2)
    {
        return thisValue >= Mathf.Min(value1, value2) && thisValue <= Mathf.Max(value1, value2);
    }

    #region Stats panel
    
    [SerializeField] TMP_Text directionText;
    [SerializeField] TMP_Text shotWidthDeflectionText;
    [SerializeField] TMP_Text shotHeightDeflectionText;
    [SerializeField] TMP_Text shotTimeText;
    [SerializeField] TMP_Text shotResultText;

    void SetShotStatsPanel(string direction , float xDeflation , float yDeflation , float timeShotwasFired , string shotState) 
    {
        directionText.text = direction;
        shotWidthDeflectionText.text = xDeflation.ToString();
        shotHeightDeflectionText.text = yDeflation.ToString();
        shotTimeText.text = timeShotwasFired.ToString();
        shotResultText.text = shotState;
    }

    

    #endregion

}



