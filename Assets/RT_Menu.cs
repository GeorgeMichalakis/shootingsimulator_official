using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class RT_Menu : MonoBehaviour
{
    [SerializeField] GameObject panel01;
    [SerializeField] GameObject shotTraceCloseupPanel;


    // Start is called before the first frame update
    void Start()
    {
        //SetAccuracyBar();
        //SetTimeBar();
        //SetTraceSpeedGraph();
       // SetxDeflationGraph();
       // SetyDeflationGraph();

        for (int i = 1; i < Display.displays.Length; i++)
        {
            Display.displays[i].Activate();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Panel 1

    //user features
    public void CenterOfMassBtn() { }
    public void LomahBtn() { }
    public void DdiBtn() { }




  
    List<int> GenerateRandSet(int elementCount ,int maxValue) 
    {
        List<int> barElementValues = new List<int>();
        for (int i = 0; i < elementCount; i++)
        {
            int randx = UnityEngine.Random.Range(0, maxValue);
            while (barElementValues.Contains(randx))
            {
                randx = UnityEngine.Random.Range(0, maxValue);
            }
            barElementValues.Add(randx);
        }
        return barElementValues;
    }

    #endregion

    #region Panel 2
    public RT_BarChart traceSpeed;
    public RT_BarChart xDeflection;
    public RT_BarChart yDeflection;
    void SetTraceSpeedGraph() 
    {
         List<int> timerlist = new List<int>() { -2, -1, 0,1, 2,3,4,5,6 };
         List<int> heightlist = new List<int>() { 9, 7 ,6 ,4 ,7 ,7,9,8 ,9 };
        traceSpeed.InitGraph(timerlist, heightlist);
    }

    void SetxDeflationGraph()
    {
        List<int> timerlist = new List<int>() { -2, -1, 0, 1, 2, 3, 4, 5, 6 };
        List<int> heightlist = new List<int>() { 9, 7, 6, 4, 7, 7, 9, 8, 9 };
        xDeflection.InitGraph(timerlist, heightlist);
    }
    void SetyDeflationGraph()
    {
        List<int> timerlist = new List<int>() { -2, -1, 0, 1, 2, 3, 4, 5, 6 };
        List<int> heightlist = new List<int>() { 9, 7, 6, 4, 7, 7, 9, 8, 9 };
        yDeflection.InitGraph(timerlist, heightlist);
    }
    #endregion
}
