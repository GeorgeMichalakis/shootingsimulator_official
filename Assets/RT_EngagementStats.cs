using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class RT_EngagementStats : MonoBehaviour
{

    int maxShots = 10;
    float maxTime = 2f;

    [SerializeField] TMP_Text successRateText;
    [SerializeField] Image successRateImage;
    [SerializeField] TMP_Text executionTimeText;
    [SerializeField] Image executionTimeImage;
    [SerializeField] Image totalShotsImage;
    [SerializeField] TMP_Text totalShotsText;


    // Start is called before the first frame update
    void Start()
    {
        SetEngagementStats(0.0f, 0.0f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetEngagementStats(float successRate , float executionTime, int totalShots)
    {
        successRateText.text = successRate.ToString("F1") + "%";
        successRateImage.fillAmount = successRate / 100f;


        float timeSpentPct = (executionTime * 100) / (maxTime * 100);

        //executionTimeText.text = executionTime.ToString("") ;
        //executionTimeText.text = TimeSpan.FromSeconds((double)executionTime).ToString("mm:ss");
        var ts = TimeSpan.FromSeconds(executionTime);
        executionTimeText.text = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
        executionTimeImage.fillAmount = timeSpentPct;

        float totalShotsPct = (float) (totalShots*100) / (maxShots*100);

        totalShotsText.text = totalShots.ToString() ;
        totalShotsImage.fillAmount = totalShotsPct ;

    }



}
