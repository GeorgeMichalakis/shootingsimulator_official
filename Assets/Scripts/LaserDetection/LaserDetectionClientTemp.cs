﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using ENet;
using System.Runtime.InteropServices;


public class LaserDetectionClientTemp : MonoBehaviour
{   
    Host server;

    ENet.Event netEvent;

    public static Vector2 laserPos;
    public static bool pendingPosition = false;
    
    void Start()
    {
        Debug.Log("Start: " + name);

        bool initGood = ENet.Library.Initialize();

        if (initGood)
        {
            Debug.Log("Enet initialized");
        }
        else
        {
            Debug.Log("Enet failed to init");
        }

        //const string ip = "192.168.2.3";
        string ip = System.Net.Dns.GetHostName();
        const ushort port = 6005;
        const int maxClients = 2;
        const float keepAlivePeriodSec = 0.2f;

        Address address = new Address();
        address.SetIP(ip);
        address.Port = port;
        server = new Host();
        server.Create(address, maxClients, 1);

        InvokeRepeating(nameof(SendKeepAlive), 0f, keepAlivePeriodSec);
    }


    void Update()
    {
        //resetting laser received status
        pendingPosition = false;

        if (server.CheckEvents(out netEvent) <= 0)
        {
            if (server.Service(0, out netEvent) <= 0)
            {
                return;
            }
        }

        switch (netEvent.Type)
        {
            case ENet.EventType.Receive:
                int size = netEvent.Packet.Length;
                byte[] receivedData = new byte[size];
                Marshal.Copy(netEvent.Packet.Data, receivedData, 0, size);
                string s = "";
                for (int i = 0; i < size; ++i)
                {
                    s += receivedData[i].ToString("X2") + " ";
                }

                float shootX = System.BitConverter.ToSingle(receivedData, 0);
                float shootY = System.BitConverter.ToSingle(receivedData, 4);
                //Debug.Log("From " + netEvent.Peer.IP + ": " + s + " which is X: " + shootX + " and Y: " + shootY);
                //GetComponent<AudioSource>().Play();
                netEvent.Packet.Dispose();
                //gameObject.SendMessage("OnLaserDetected", new Vector2(shootX, shootY));
                laserPos = new Vector2(shootX, shootY);
                pendingPosition = true;
                break;
            case ENet.EventType.Connect:
                Debug.Log("Connected: " + netEvent.Peer.IP);
                break;
        }
        
    }


    void SendKeepAlive()
    {
        byte[] data = new byte[1];
        data[0] = 0x21;
        Packet iAmAlivePacket = new Packet();
        iAmAlivePacket.Create(data, 1, PacketFlags.Reliable);

        server.Broadcast(0, ref iAmAlivePacket);
    }

    private void OnApplicationQuit()
    {
        server.Flush();
        ENet.Library.Deinitialize();
        Debug.Log("Enet deinitialized");
    }



}