using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class RT_ShotTrace : MonoBehaviour
{
    public int engagementIndex;
    [SerializeField]public  TMP_Text executionTimeText;
    public Image bg;

    public Image shotMarker;
    public Image targetImage;
    public Image targetShotImage;

    public Transform targetCenter;


    public float timeBulletWasShot = 0;
    public Button shotTraceBtn;

    // Start is called before the first frame update
    void Start()
    {
        executionTimeText.text = "0";
        bg = GetComponent<Image>();
        shotMarker.enabled = false;
        targetShotImage.enabled = false;
        shotTraceBtn.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
